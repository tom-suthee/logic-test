if (process.argv.length !== 2) {
  console.log('usage: node --experimental-modules common.mjs')
  process.exit()
}

import util from './util.mjs'

function mergeSort(input) {
  if (input.length <= 1) {
    return input
  } else {
    const middle = Math.floor(input.length / 2)
    return merge(
      mergeSort(input.slice(0, middle)), 
      mergeSort(input.slice(middle))
    );
  }
}

function merge (left, right) {
  let resultArray = [], leftIndex = 0, rightIndex = 0

  while (leftIndex < left.length && rightIndex < right.length) {
    if (left[leftIndex] < right[rightIndex]) {
      resultArray.push(left[leftIndex])
      leftIndex++
    } else {
      resultArray.push(right[rightIndex])
      rightIndex++
    }
  }

  return resultArray
          .concat(left.slice(leftIndex))
          .concat(right.slice(rightIndex));
}

util.track(() => {
  console.log(mergeSort(util.random_1000))
})
