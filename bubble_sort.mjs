if (process.argv.length !== 2) {
  console.log('usage: node --experimental-modules common.mjs')
  process.exit()
}

import util from './util.mjs'

function bubbleSort (input) {
  let len = input.length;
  for (let i = 0; i < len; i++) {
      for (let j = 0; j < len; j++) {
          if (input[j] > input[j + 1]) {
              let tmp = input[j];
              input[j] = input[j + 1];
              input[j + 1] = tmp;
          }
      }
  }
  return input;
};

util.track(() => {
  console.log(bubbleSort(util.random_1000))
})
