if (process.argv.length !== 3) {
  console.log('usage: node --experimental-modules fibo.mjs [number]')
  process.exit()
}

let n = parseInt(process.argv[2])
if (n <= 0) {
  console.log('usage: node --experimental-modules fibo.mjs [number]')
  process.exit()
}

import util from './util.mjs'

function fibo(n) {
  let val = 0;
  if (n <= 1) {
    return [ 1 ]
  } else if (n === 2) {
    return [ 1, 1 ]
  } else {
    let tmp = fibo(n - 1)
    //return [ tmp[0] + tmp[1] ].concat(tmp);                         // reversed
    return tmp.concat([ tmp[tmp.length - 1] + tmp[tmp.length - 2] ])  // normal
  }
}

util.track(() => {
  console.log(fibo(n))
})
